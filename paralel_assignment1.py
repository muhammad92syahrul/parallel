import json
import re
import string
from sys import exit as e
import pandas as pd
from mpi4py import MPI
import time
from math import ceil


def strip_links(text):
    link_regex = re.compile(
        r'((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', re.DOTALL)
    links = re.findall(link_regex, text)
    for link in links:
        text = text.replace(link[0], ', ')
    return text


def strip_all_entities(text):
    entity_prefixes = ['!', '?', ',', '.', "'", '"']
    for separator in string.punctuation:
        if separator in entity_prefixes:
            text = text.replace(separator, ' ')
    return text


def main():

    comm = MPI.COMM_WORLD
    comm_rank = comm.Get_rank()
    comm_size = comm.Get_size()

    if comm_rank == 0:
        starttime = time.time()
        print("number of cores = "+str(comm_size))

        # Opening Grid Melbourne

        melb_grid = open("./parallel_/files/melbGrid2021.json", 'r')
        n_grid = 16

        # read geo json
        data = json.load(melb_grid)

        # grid_cell =
        key = []
        value = []
        for cell in range(n_grid):
            b = []
            m = data['features'][cell]['properties']
            key.append(m['id'])
            value.append(m)

        grid_cell = dict(zip(key, value))
        # Closing file
        melb_grid.close()

        test_twitter = open("./parallel_/files/smallTwitter2021.json", 'r')
        twitter = json.load(test_twitter)
        # print(type(twitter))
        # e(0)
        cord_tweets = []
        for x in twitter['rows']:
            a = x['value']['geometry']
            raw_b = x['value']['properties']['text']

            # print("\n")
            b = strip_links(raw_b)
            b = strip_all_entities(b)
            b = b.lower().strip()

            cord_tweets.append({"text_raw": raw_b, "text": b,
                                "point": a['coordinates']})
            # print(cord_tweets)
            # e(0)

        i = ceil(len(cord_tweets)/comm_size)
        cord_tweet = []
        for x in range(0, len(cord_tweets), i):
            if (x+1) < len(cord_tweets):
                cord_tweet.append(cord_tweets[x:(x+i)])
            else:
                cord_tweet.append(cord_tweets[x:len(cord_tweets)])

        # Closing file
        test_twitter.close()

        list_word = open("./parallel_/files/AFINN.txt", "r")
        word = list_word.readlines()
        list_value_word = []
        value = []
        key = []
        for kata in word:
            m = kata.split()
            if len(m) > 2:
                v = (' '.join((m[0:(len(m)-1)])))
                key.append(v.strip())
            else:
                key.append(m[0].strip())
            a = m[-1].strip()
            value.append(int(a))

        dict_word = dict(zip(key, value))

    else:
        cord_tweet = None
        grid_cell = None
        dict_word = None

    cord_tweet = comm.scatter(cord_tweet, root=0)
    dict_word = comm.bcast(dict_word, root=0)
    grid_cell = comm.bcast(grid_cell, root=0)

    a1 = 0
    a2 = 0
    a3 = 0
    a4 = 0
    b1 = 0
    b2 = 0
    b3 = 0
    b4 = 0
    c1 = 0
    c2 = 0
    c3 = 0
    c4 = 0
    c5 = 0
    d3 = 0
    d4 = 0
    d5 = 0

    score_a1 = 0
    score_a2 = 0
    score_a3 = 0
    score_a4 = 0
    score_b1 = 0
    score_b2 = 0
    score_b3 = 0
    score_b4 = 0
    score_c1 = 0
    score_c2 = 0
    score_c3 = 0
    score_c4 = 0
    score_c5 = 0
    score_d3 = 0
    score_d4 = 0
    score_d5 = 0

    for c in cord_tweet:
        # A1
        split_tweet = c['text'].split()

        if (c['point'][0] <= grid_cell['A1']['xmax'] and c['point'][0] > grid_cell['A1']['xmin']) and (c['point'][1] <= grid_cell['A1']['ymax'] and c['point'][1] >= grid_cell['A1']['ymin']):
            a1 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_a1 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['A2']['xmax'] and c['point'][0] > grid_cell['A2']['xmin']) and (c['point'][1] <= grid_cell['A2']['ymax'] and c['point'][1] >= grid_cell['A2']['ymin']):
            a2 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_a2 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['A3']['xmax'] and c['point'][0] > grid_cell['A3']['xmin']) and (c['point'][1] <= grid_cell['A3']['ymax'] and c['point'][1] >= grid_cell['A3']['ymin']):
            a3 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_a3 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['A4']['xmax'] and c['point'][0] > grid_cell['A4']['xmin']) and (c['point'][1] <= grid_cell['A4']['ymax'] and c['point'][1] >= grid_cell['A4']['ymin']):
            a4 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_a4 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['B1']['xmax'] and c['point'][0] > grid_cell['B1']['xmin']) and (c['point'][1] <= grid_cell['B1']['ymax'] and c['point'][1] >= grid_cell['B1']['ymin']):
            b1 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_b1 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['B2']['xmax'] and c['point'][0] > grid_cell['B2']['xmin']) and (c['point'][1] <= grid_cell['B2']['ymax'] and c['point'][1] >= grid_cell['B2']['ymin']):
            b2 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_b2 += dict_word[txt]
                    # if score_b2 < 0:
                    #     print(dict_word[txt])
        elif (c['point'][0] <= grid_cell['B3']['xmax'] and c['point'][0] > grid_cell['B3']['xmin']) and (c['point'][1] <= grid_cell['B3']['ymax'] and c['point'][1] >= grid_cell['B3']['ymin']):
            b3 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_b3 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['B4']['xmax'] and c['point'][0] > grid_cell['B4']['xmin']) and (c['point'][1] <= grid_cell['B4']['ymax'] and c['point'][1] >= grid_cell['B4']['ymin']):
            b4 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_b4 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['C1']['xmax'] and c['point'][0] > grid_cell['C1']['xmin']) and (c['point'][1] <= grid_cell['C1']['ymax'] and c['point'][1] >= grid_cell['C1']['ymin']):
            c1 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_c1 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['C2']['xmax'] and c['point'][0] > grid_cell['C2']['xmin']) and (c['point'][1] <= grid_cell['C2']['ymax'] and c['point'][1] >= grid_cell['C3']['ymin']):
            c2 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_c2 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['C3']['xmax'] and c['point'][0] > grid_cell['C3']['xmin']) and (c['point'][1] <= grid_cell['C3']['ymax'] and c['point'][1] >= grid_cell['C3']['ymin']):
            c3 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_c3 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['C4']['xmax'] and c['point'][0] > grid_cell['C4']['xmin']) and (c['point'][1] <= grid_cell['C4']['ymax'] and c['point'][1] >= grid_cell['C4']['ymin']):
            c4 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_c4 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['C5']['xmax'] and c['point'][0] > grid_cell['C5']['xmin']) and (c['point'][1] <= grid_cell['C5']['ymax'] and c['point'][1] >= grid_cell['C5']['ymin']):
            c5 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_c5 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['D3']['xmax'] and c['point'][0] > grid_cell['D3']['xmin']) and (c['point'][1] <= grid_cell['D3']['ymax'] and c['point'][1] >= grid_cell['D3']['ymin']):
            d3 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_d3 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['D4']['xmax'] and c['point'][0] > grid_cell['D4']['xmin']) and (c['point'][1] <= grid_cell['D4']['ymax'] and c['point'][1] >= grid_cell['D4']['ymin']):
            d4 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_d4 += dict_word[txt]
        elif (c['point'][0] <= grid_cell['D5']['xmax'] and c['point'][0] > grid_cell['D5']['xmin']) and (c['point'][1] <= grid_cell['D5']['ymax'] and c['point'][1] >= grid_cell['D5']['ymin']):
            d5 += 1
            for txt in split_tweet:
                if txt in dict_word.keys():
                    score_d5 += dict_word[txt]

    a1 = comm.reduce(a1, op=MPI.SUM, root=0)
    a2 = comm.reduce(a2, op=MPI.SUM, root=0)
    a3 = comm.reduce(a3, op=MPI.SUM, root=0)
    a4 = comm.reduce(a4, op=MPI.SUM, root=0)
    b1 = comm.reduce(b1, op=MPI.SUM, root=0)
    b2 = comm.reduce(b2, op=MPI.SUM, root=0)
    b3 = comm.reduce(b3, op=MPI.SUM, root=0)
    b4 = comm.reduce(b4, op=MPI.SUM, root=0)
    c1 = comm.reduce(c1, op=MPI.SUM, root=0)
    c2 = comm.reduce(c2, op=MPI.SUM, root=0)
    c3 = comm.reduce(c3, op=MPI.SUM, root=0)
    c4 = comm.reduce(c4, op=MPI.SUM, root=0)
    c5 = comm.reduce(c5, op=MPI.SUM, root=0)
    d3 = comm.reduce(d3, op=MPI.SUM, root=0)
    d4 = comm.reduce(d4, op=MPI.SUM, root=0)
    d5 = comm.reduce(d5, op=MPI.SUM, root=0)

    score_a1 = comm.reduce(score_a1, op=MPI.SUM, root=0)
    score_a2 = comm.reduce(score_a2, op=MPI.SUM, root=0)
    score_a3 = comm.reduce(score_a3, op=MPI.SUM, root=0)
    score_a4 = comm.reduce(score_a4, op=MPI.SUM, root=0)
    score_b1 = comm.reduce(score_b1, op=MPI.SUM, root=0)
    score_b2 = comm.reduce(score_b2, op=MPI.SUM, root=0)
    score_b3 = comm.reduce(score_b3, op=MPI.SUM, root=0)
    score_b4 = comm.reduce(score_b4, op=MPI.SUM, root=0)
    score_c1 = comm.reduce(score_c1, op=MPI.SUM, root=0)
    score_c2 = comm.reduce(score_c2, op=MPI.SUM, root=0)
    score_c3 = comm.reduce(score_c3, op=MPI.SUM, root=0)
    score_c4 = comm.reduce(score_c4, op=MPI.SUM, root=0)
    score_c5 = comm.reduce(score_c5, op=MPI.SUM, root=0)
    score_d3 = comm.reduce(score_d3, op=MPI.SUM, root=0)
    score_d4 = comm.reduce(score_d4, op=MPI.SUM, root=0)
    score_d5 = comm.reduce(score_d5, op=MPI.SUM, root=0)

    if comm_rank == 0:
        total_tweet = [a1, a2, a3, a4, b1, b2, b3,
                       b4, c1, c2, c3, c4, c5, d3, d4, d5]
        score_sentiment = [score_a1, score_a2, score_a3, score_a4, score_b1, score_b2, score_b3,
                           score_b4, score_c1, score_c2, score_c3, score_c4, score_c5, score_d3, score_d4, score_d5]

        result = {'Cell': grid_cell.keys(),
                  'Total Tweet': total_tweet,
                  'Overall Sentiment Score': score_sentiment}

        df = pd.DataFrame(result)
        print("===================answer===============")
        print(df)
        endtime = time.time()
        print("Executing time: %.6f" % (endtime-starttime))


if __name__ == '__main__':
    # cara menjalankan :
    # mpirun -np x python paralel_assignment1.py
    # x diganti angka 1,2,3 (jumlah core)
    main()
